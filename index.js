var rsync = require('rsync');
var glob = require("glob")
var fs = require("fs");
var path = require("path");
var ps = require('ps-node');
var df = require('node-df');
var parallel = 0;
var max_parallel = 6;

var server = process.argv[process.argv.length - 1];

function download_files() {
  var get_files = new rsync()
    .set('progress')
    .set('list-only')
    .set('min-size', '101g')
    .source('vatsan@' + server + ':chia-plots/*.plot');

  get_files.execute(function (error, code, cmd) {}, function (data) {
    var lines = data.toString().split("\n");
    lines.forEach(line => {
      var filename = line.split(" ")[4];
      if (filename && filename.match(".plot")) {
        download_file(filename);
      }
    });
    // do things like parse progress
  }, function (error) {
    console.error(error);
    // do things like parse error output
  });
}
      download_files();

setInterval(function () {
  ps.lookup({
    command: 'rsync',
    arguments: server,
  }, function (err, resultList) {
    if (resultList && resultList.length == 0) {
      parallel = 0;
      download_files();
}
  });
}, 20000);

function download_file(filename) {
  if (parallel < max_parallel) {
    glob("/mnt/*/." + filename + "*", {}, function (er, files) {
      if (files.length) {
        console.log(filename + " already being downloaded to . " + files[0] + " - " + (fs.statSync(files[0]).size / 1024 / 1024 / 1024) + "G done.");
      } else {
        glob("/mnt/*/" + filename + "*", {}, function (er, files) {
          if (files.length && (fs.statSync(files[0]).size / 1024 / 1024 / 1024 > 100)) {
            console.warn(filename + " already exists at " + files[0]);
            console.log("DELETE:");
            console.warn('\x1b[36m%s\x1b[0m', 'rm -rf /mnt/*/' + filename);
            console.log("\n");
          } else if (parallel < max_parallel) {
            parallel++;
            df(function (error, drives) {
              if (drives) {
                var available_drives = [];
                drives.sort(function (a, b) {
                  return parseFloat(b.available) - parseFloat(a.available);
                });
                drives.forEach(drive => {
                  if (drive.filesystem.match("/dev") && drive.available > 102 * 1024 * 1024) {
                    available_drives.push(drive.mount);
                  }
                });
                available_drives = available_drives.sort(() => Math.random() - 0.5)
                console.log("Preparing to rsync " + filename + " to " + available_drives[0]);
                var download = new rsync()
                  .set('progress')
                  .set('remove-source-files')
                  .flags('va')
                  .set('min-size', '101g')
                  .source('vatsan@' + server + ':chia-plots/' + filename)
                  .destination(available_drives[0] + '/');
                console.log(download.command());

                download.execute(function (error, code, cmd) {}, function (data) {
                  console.log(data.toString());
                }, function (error) {
                  console.log("Finished");
                  parallel--;
                  // do things like parse error output
                });
              }
            });
          }
        });
      }
    });
  }
}